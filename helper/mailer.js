const nodemailer = require("nodemailer");
const { OAuth2Client } = require("google-auth-library");

module.exports = async (receiver, title, messageToBeSent) => {
  const { CLIENT_ID, CLIENT_SECRET, REFRESH_TOKEN } = process.env;

  const client = new OAuth2Client(CLIENT_ID, CLIENT_SECRET);

  client.setCredentials({
    refresh_token: REFRESH_TOKEN,
  });

  const token = await client.getAccessToken();
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: "ihim512@gmail.com",
      clientId: client._clientId,
      clientSecret: client._clientSecret,
      refreshToken: REFRESH_TOKEN,
      accessToken: token,
    },
  });

  const mailOptions = {
    from: "ihim512@gmail.com",
    // to: `uk.name2018@gmail.com`,
    to: `${receiver}`,
    // subject: "Sapa",
    subject: `${title}`,
    // text: `Text`,
    text: `${messageToBeSent}`,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log("error: " + error);
    } else return info;
  });
};
