// const day = new Date().toLocaleDateString().split("/").join("");
module.exports = {
  encryption: (salt, code = "code") => {
    const textToChars = (text) => text.split("").map((c) => c.charCodeAt(0));
    const byteHex = (n) => ("0" + Number(n).toString(16)).substring(-2);
    const applySaltToChar = (c) => textToChars(code).reduce((a, b) => a ^ b, c);
    return salt
      .split("")
      .map(textToChars)
      .map(applySaltToChar)
      .map(byteHex)
      .join("")
      .toString();
  },
  decryption: (salt, code = "code") => {
    const textToChars = (text) => text.split("").map((c) => c.charCodeAt(0));
    const applySaltToChar = (c) => textToChars(code).reduce((a, b) => a ^ b, c);
    return salt
      .match(/.{1,2}/g)
      .map((hex) => parseInt(hex, 16))
      .map(applySaltToChar)
      .map((charCode) => String.fromCharCode(charCode))
      .join("");
  },
};
