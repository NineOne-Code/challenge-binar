"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return queryInterface.bulkInsert("UserBiodata", [
      {
        name: "Nine One",
        age: 19,
        gender: "m",
        level: 99,
        userId: 1,
        avatar: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "One One",
        age: 23,
        gender: "m",
        level: 6,
        userId: 2,
        avatar: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ibrahim Nagib",
        age: 19,
        gender: "m",
        level: 90,
        userId: 3,
        avatar: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("UserBiodata", null, {});
  },
};
