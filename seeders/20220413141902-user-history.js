"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return queryInterface.bulkInsert("UserHistories", [
      {
        time: "07:40:20",
        score: 70,
        userId: 1,
        video: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        time: "08:21:32",
        score: 87,
        userId: 2,
        video: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        time: "14:10:20",
        score: 90,
        userId: 3,
        video: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("UserHistories", null, {});
  },
};
