"use strict";

const bcrypt = require("bcryptjs");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return queryInterface.bulkInsert("Users", [
      {
        id: 1,
        username: "nine",
        email: "member001@gmail.com",
        password: await bcrypt.hash("12345678", 10),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 2,
        username: "one",
        email: "member002@gmail.com",
        password: await bcrypt.hash("12345678", 10),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 3,
        username: "ibrahim",
        email: "member003@gmail.com",
        password: await bcrypt.hash("12345678", 10),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("Users", null, {});
  },
};
