const { User, UserHistory, UserBiodata } = require("../models");
const fs = require("fs");
const config = require("../helper/mediaPath");
module.exports = {
  getAllHistories: async (req, res) => {
    try {
      const histories = await UserHistory.findAll({
        attributes: ["time", "score"],
        include: {
          model: User,
          attributes: ["username"],
        },
      });
      res.status(200).json({
        message: `Success get All Histories.`,
        data: histories,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  getHistory: async (req, res) => {
    try {
      const username = req.headers.username;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });
      const histories = await UserHistory.findAll({
        attributes: ["time", "score", "video"],
        where: {
          userId: user.id,
        },
      });
      res.status(200).json({
        message: `Success get Your Histories.`,
        data: histories,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  addHistory: async (req, res) => {
    try {
      const { score } = req.body;
      const username = req.headers.username;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      if (req.file) {
        const video = req.file.path;
        console.log(video);

        await UserHistory.create({
          score: score,
          time: new Date().toLocaleTimeString().split(" ")[0],
          userId: user.id,
          video: video,
        });
        res.status(201).json({
          message: `Success add Your Record.`,
        });
      } else {
        await UserHistory.create({
          score: score,
          time: new Date().toLocaleTimeString().split(" ")[0],
          userId: user.id,
        });
        res.status(201).json({
          message: `Success add Your Record.`,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  updateHistory: async (req, res) => {
    try {
      const { id } = req.params;
      const { score } = req.body;
      const username = req.headers.username;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      user.video = await UserHistory.findOne({
        where: {
          id: id,
        },
      });
      console.log(user);
      user.video = user.video.video;

      if (req.file) {
        const video = req.file.path;
        const availableVideo = `${config.rootPath}/${user.video}`;
        // console.log(user);

        if (fs.existsSync(availableVideo)) {
          fs.unlinkSync(availableVideo);
        }

        await UserHistory.update(
          {
            score: score,
            time: new Date().toLocaleTimeString().split(" ")[0],
            video: video,
          },
          {
            where: {
              id: id,
              userId: user.id,
            },
          }
        );
        res.status(200).json({
          message: `Success update Your Record.`,
        });
      } else {
        await UserHistory.update(
          {
            score: score,
            time: new Date().toLocaleTimeString().split(" ")[0],
          },
          {
            where: {
              id: id,
              userId: user.id,
            },
          }
        );
        res.status(200).json({
          message: `Success update Your Record.`,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  deleteHistory: async (req, res) => {
    try {
      const { id } = req.params;
      await UserHistory.destroy({
        where: {
          id: id,
        },
      });
      res.status(200).json({
        message: `Success delete Your Record.`,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
};
