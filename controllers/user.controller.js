"use strict";
const { decryption } = require("../helper/encrypt");
const { User, UserHistory, UserBiodata } = require("../models");

module.exports = {
  getAllUsers: async (req, res) => {
    try {
      const users = await User.findAll({
        attributes: ["username"],
        include: [
          {
            model: UserBiodata,
            as: "Biodata",
            attributes: ["name", "age", "gender", "level"],
          },
          {
            model: UserHistory,
            as: "Histories",
            attributes: ["time", "score"],
          },
        ],
      });
      if (!users) {
        return res.status(404).json({ message: "User Not Found" });
      }
      res.status(200).json({
        message: `Success get All Players.`,
        data: users,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  deleteAccount: async (req, res) => {
    try {
      const username = req.headers.username;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      const userData = await UserBiodata.findOne({
        where: {
          userId: user.id,
        },
      });
      if (userData) {
        user.avatar = userData.avatar;

        const availableImage = `${config.rootPath}/${user.avatar}`;

        if (fs.existsSync(availableImage)) {
          fs.unlinkSync(availableImage);
        }
      }

      await User.destroy({
        where: {
          username: username,
        },
      });
      res.status(200).json({
        message: `Success DELETE Account ${username}.`,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
};
