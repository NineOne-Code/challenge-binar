"use strict";
require("dotenv").config();
const { User, UserHistory, UserBiodata } = require("../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mailer = require("../helper/mailer");

module.exports = {
  signIn: async (req, res) => {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({
        attributes: ["id", "username", "password"],
        where: {
          username: username,
        },
        as: "Users",
      });
      console.log(user);
      if (user) {
        const isValidPassword = await bcrypt.compare(password, user.password);
        if (isValidPassword) {
          const token = jwt.sign(
            {
              id: user.id,
              username: user.username,
            },
            process.env.JWT
          );
          res.status(201).json({
            message: `User ${user.username} is available, with ID ${user.id}.`,
            token: token,
          });
        } else {
          return res.status(403).json({ message: "Wrong Password" });
        }
      } else {
        return res.status(404).json({ message: "User Not Found" });
      }
    } catch (error) {
      console.log(error.message);
      if (
        error.message ==
          `Cannot read properties of null (reading 'username')` ||
        error.message === `Cannot read property 'username' of null` ||
        error.message ===
          `WHERE parameter \"username\" has invalid \"undefined\" value`
      ) {
        return res.status(404).json({ message: "Username Not Found" });
      }
      if (
        error.message ===
        `WHERE parameter \"password\" has invalid \"undefined\" value`
      ) {
        return res.status(403).json({ message: "Wrong Password" });
      }
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
    }
  },
  signUp: async (req, res) => {
    try {
      const { username, email, password } = req.body;
      console.log(username, password);
      const user = await User.create({
        username: username,
        email: email,
        password: password,
      });
      const token = jwt.sign(
        {
          id: user.id,
          username: user.username,
        },
        process.env.JWT
      );
      const sendMail = await mailer(
        email,
        "Welcome to Games System",
        `Welcome User ${user.username} to We're server, your ID is ${user.id}.`
      );
      res.status(201).json({
        message: `Welcome User ${user.username} to We're server, your ID is ${user.id}.`,
        token: token,
      });
      console.log(sendMail);
    } catch (error) {
      if (error.message === `username must be unique`) {
        return res.status(409).json({ message: "Username already registered" });
      }
      if (
        error.message ===
        `null value in column \"username\" violates not-null constraint`
      ) {
        return res.status(409).json({ message: "Username required" });
      }
      if (
        error.message ===
        `null value in column \"password\" violates not-null constraint`
      ) {
        return res.status(409).json({ message: "Password required" });
      }
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
    }
  },
  requestOTP: async (req, res) => {
    try {
      const { email } = req.body;
      const digits = "0123456789";
      let code = "";

      for (let i = 0; i < 6; i++) {
        code += digits[Math.floor(Math.random() * 10)];
        console.log(code);
      }

      const salt = bcrypt.genSaltSync(10);

      const hashCode = bcrypt.hashSync(code, salt);

      const user = await User.update(
        {
          otp: `${hashCode}`,
        },
        {
          where: {
            email: `${email}`,
          },
          returning: true,
        }
      );

      if (!user[0]) return res.status(404).json({ message: "User Not Found" });

      await mailer(
        email,
        "OTP Games System",
        `Your OTP ${code} from Games System`
      );

      return res.status(200).json({
        message: `Your OTP ${code} sent to ${email}`,
      });
    } catch (error) {
      if (error.message === `username must be unique`) {
        return res.status(409).json({ message: "Username already registered" });
      }
      if (
        error.message ===
        `null value in column \"username\" violates not-null constraint`
      ) {
        return res.status(409).json({ message: "Username required" });
      }
      if (
        error.message ===
        `null value in column \"password\" violates not-null constraint`
      ) {
        return res.status(409).json({ message: "Password required" });
      }
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
    }
  },
  forgotPassword: async (req, res) => {
    try {
      const { email, newPassword, newPasswordConfirmation, otp } = req.body;
      if (newPassword !== newPasswordConfirmation)
        return res.status(400).json({
          message: `New Password not match`,
        });
      const user = await User.findOne({ where: { email: email } });
      if (!user) return res.status(404).json({ message: "User Not Found" });
      const isValidOTP = await bcrypt.compare(otp, user.otp);

      if (!isValidOTP) return res.status(400).json({ message: "Wrong OTP" });
      const salt = bcrypt.genSaltSync(10);

      const hashPassword = bcrypt.hashSync(newPassword, salt);
      await user.update({ password: hashPassword, otp: null });

      const sendMail = await mailer(
        email,
        "Success Change Password",
        `Password has been changed`
      );
      res.status(200).json({
        message: `Password has been changed`,
      });
    } catch (error) {
      if (error.message === `username must be unique`) {
        return res.status(409).json({ message: "Username already registered" });
      }
      if (
        error.message ===
        `null value in column \"username\" violates not-null constraint`
      ) {
        return res.status(409).json({ message: "Username required" });
      }
      if (
        error.message ===
        `null value in column \"password\" violates not-null constraint`
      ) {
        return res.status(409).json({ message: "Password required" });
      }
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
    }
  },
};
