const { User, UserHistory, UserBiodata } = require("../models");
const fs = require("fs");
const config = require("../helper/mediaPath");
module.exports = {
  getUser: async (req, res) => {
    try {
      const username = req.headers.username;
      const user = await User.findOne({
        attributes: ["username"],
        where: {
          username: username,
        },
        include: [
          {
            model: UserBiodata,
            as: "Biodata",
            attributes: ["name", "age", "gender", "level", "avatar"],
          },
          {
            model: UserHistory,
            as: "Histories",
            attributes: ["time", "score", "video"],
          },
        ],
      });
      res.status(200).json({
        message: `Success get Player.`,
        data: user,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  addData: async (req, res) => {
    try {
      const username = req.headers.username;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      const { name, age, level, gender } = req.body;
      if (req.file) {
        const avatar = req.file.path;

        await UserBiodata.create({
          // attributes: ["name", "age", "level", "gender"],
          name: name,
          age: +age,
          level: +level,
          gender: gender,
          userId: user.id,
          avatar: avatar,
        });
        res.status(201).json({
          message: `Success add Data Player.`,
        });
      } else {
        await UserBiodata.create({
          // attributes: ["name", "age", "level", "gender"],
          name: name,
          age: +age,
          level: +level,
          gender: gender,
          userId: user.id,
        });
        res.status(201).json({
          message: `Success add Data Player.`,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  updateData: async (req, res) => {
    try {
      const username = req.headers.username;

      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });
      user.avatar = await UserBiodata.findOne({
        attributes: ["avatar"],
        where: {
          userId: user.id,
        },
      });
      user.avatar = user.avatar.avatar;
      const { name, age, gender, level } = req.body;

      if (req.file) {
        //  let currentImage = user.path;
        const avatar = req.file.path;
        const availableImage = `${config.rootPath}/${user.avatar}`;
        console.log(user);

        if (fs.existsSync(availableImage)) {
          fs.unlinkSync(availableImage);
        }

        await UserBiodata.update(
          {
            name: name,
            age: age,
            gender: gender,
            level: level,
            avatar: avatar,
          },
          {
            where: {
              userId: user.id,
            },
            returning: true,
          }
        );
        res.status(200).json({
          message: `Success Update data Player.`,
        });
      } else {
        await UserBiodata.update(
          {
            name: name,
            age: age,
            gender: gender,
            level: level,
          },
          {
            where: {
              userId: user.id,
            },
            returning: true,
          }
        );
        res.status(200).json({
          message: `Success Update data Player.`,
        });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
  deleteData: async (req, res) => {
    try {
      const username = req.headers.username;
      const user = await User.findOne({
        attributes: ["id", "username"],
        where: {
          username: username,
        },
      });

      user.avatar = await UserBiodata.findOne({
        attributes: ["avatar"],
        where: {
          id: user.id,
        },
      });
      user.avatar = user.avatar.avatar;

      const availableImage = `${config.rootPath}/${user.avatar}`;

      if (fs.existsSync(availableImage)) {
        fs.unlinkSync(availableImage);
      }

      await UserBiodata.destroy({
        where: {
          userId: user.id,
        },
      });
      res.status(200).json({
        message: `Success delete Data Player.`,
      });
    } catch (error) {
      res
        .status(500)
        .json({ message: error.message || "Internal Server Error" });
      console.log(error);
    }
  },
};
