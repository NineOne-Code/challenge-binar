require("dotenv").config();

const { DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOSTNAME } = process.env;

module.exports = {
  development: {
    username: "postgres",
    password: "123",
    database: "games-development",
    host: "127.0.0.1",
    dialect: "postgres",
  },
  test: {
    username: "postgres",
    password: "123",
    database: "games-test",
    host: "127.0.0.1",
    dialect: "postgres",
  },
  production: {
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOSTNAME,
    dialect: "postgres",
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
  },
};
