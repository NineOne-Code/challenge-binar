const swaggerAutogen = require("swagger-autogen")();

const doc = {
  info: {
    title: "Game System",
    description:
      "API Documentations Using Postman: https://documenter.getpostman.com/view/13273250/UyrBjGbS",
  },
  host: "localhost:3000",
  schemes: ["http"],
};

const outputFile = "./swagger/swagger-output.json";
const endpointsFiles = [
  "./routes/auth.routes.js",
  "./routes/user.routes.js",
  "./routes/userhistory.routes.js",
];

/* NOTE: if you use the express Router, you must pass in the 
   'endpointsFiles' only the root file where the route starts,
   such as index.js, app.js, routes.js, ... */

swaggerAutogen(outputFile, endpointsFiles, doc);
