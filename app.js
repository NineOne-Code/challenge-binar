const express = require("express");
const swagger = require("swagger-ui-express");
const swaggerOutput = require("./swagger/swagger-output.json");
const logger = require("morgan");

require("dotenv").config();

const authRouter = require("./routes/auth.routes");
const userRouter = require("./routes/user.routes");
const userHistoryRouter = require("./routes/userhistory.routes");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(logger("dev"));

app.use("/", swagger.serve);
app.get("/", swagger.setup(swaggerOutput));

app.use("/", authRouter);
app.use("/user", userRouter);
app.use("/history", userHistoryRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
  console.log(
    `API Documentations Postman: https://documenter.getpostman.com/view/13273250/UyrBjGbS`
  );
});

module.exports = app;
