const cloudinary = require("cloudinary").v2;
const fs = require("fs");

require("dotenv").config();

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

module.exports = async (req, res, next) => {
  try {
    const path = `games-api/${req.file.mimetype.split("/")[0]}`;
    const uploadResult = await cloudinary.uploader.upload(req.file.path, {
      folder: path,
    });
    fs.unlinkSync(req.file.path);
    req.body.profile_picture_url = uploadResult.secure_url;
    next();
  } catch (error) {
    fs.unlinkSync(req.file.path);
    console.log(error);
  }
};
