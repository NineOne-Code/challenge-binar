const { decryption } = require("../helper/encrypt");
const { User } = require("../models");
const jwt = require("jsonwebtoken");
module.exports = {
  authorization: async (req, res, next) => {
    try {
      if (!req.headers.authorization) {
        res.status(401).json({
          message: "Unauthorized",
        });
      } else {
        if (jwt.decode(req.headers?.authorization)) {
          const { username } = jwt.decode(req.headers?.authorization);
          req.headers.username = username;

          const user = await User.findOne({
            attributes: ["username"],
            where: {
              username: username,
            },
          });
          console.log(`user: ${username}`);
          if (user) {
            next();
          } else {
            res.status(401).json({
              message: "Unauthorized",
            });
          }
        } else {
          return res.status(401).json({
            message: "Unauthorized",
          });
        }
      }
    } catch (error) {
      next(error);
    }
  },
};
