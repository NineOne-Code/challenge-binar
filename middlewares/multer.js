const multer = require("multer");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (fs.existsSync("public")) {
      cb(null, "public");
    } else {
      fs.mkdirSync("public");
      fs.mkdirSync("public/uploads");
      fs.mkdirSync("public/uploads/image");
      fs.mkdirSync("public/uploads/video");
      cb(null, "public");
    }
  },
  filename: function (req, file, cb) {
    const isImageFile =
      file.mimetype === "image/png" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/jpeg"
        ? true
        : false;
    const uniqueSuffix = Date.now() + "-" + file.originalname;
    cb(
      null,
      "/uploads/" +
        `${isImageFile ? "image/" : "video/"}` +
        file.fieldname +
        "-" +
        uniqueSuffix
    );
  },
});

module.exports = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    // images
    if (
      file.mimetype === "image/png" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/jpeg"
    ) {
      return cb(null, true);
    }
    // video
    if (
      file.mimetype === "video/mp4" ||
      file.mimetype === "video/x-matroska" ||
      file.mimetype === "video/quicktime"
    ) {
      return cb(null, true);
    }
    cb(null, false);

    cb(new Error("Wrong format file"));
  },
});
