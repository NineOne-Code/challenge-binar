const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const app = require("../app");

require("dotenv").config();

let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync("12345678", salt);
  const user = {
    id: 1,
    username: "nine",
    password: hashPassword,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  await queryInterface.bulkInsert("Users", [user]);

  token = jwt.sign(
    {
      id: user.id,
      username: user.username,
    },
    process.env.JWT
  );
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "Users",
    {},
    { truncate: true, restartIdentity: true, cascade: true }
  );
});

describe("GET Biodata Testing", () => {
  it("Unauthorized", (done) => {
    request(app)
      .get("/user/myaccount")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("invalid token", (done) => {
    request(app)
      .get("/user/myaccount")
      .set("authorization", [...token].reverse().join(""))
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Success", (done) => {
    request(app)
      .get("/user/myaccount")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success get Player.");
          done();
        }
      });
  });
});

describe("POST Biodata Testing", () => {
  it("Success", (done) => {
    request(app)
      .post("/user/myaccount/add")
      .set("authorization", token)
      .send({
        name: "Eren Yeager",
        age: 18,
        level: 1,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success add Data Player.");
          done();
        }
      });
  });
  it("Unauthorized", (done) => {
    request(app)
      .post("/user/myaccount/add")
      .send({
        name: "Eren Yeager",
        age: 18,
        level: 1,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Invalid token", (done) => {
    request(app)
      .post("/user/myaccount/add")
      .set("authorization", [...token].reverse().join(""))
      .send({
        name: "Eren Yeager",
        age: 18,
        level: 1,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});

describe("Update Biodata Testing", () => {
  it("Success", (done) => {
    request(app)
      .put("/user/myaccount/update")
      .set("authorization", token)
      .send({
        name: "Mawang Yeager",
        age: 30,
        level: 99,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success Update data Player.");
          done();
        }
      });
  });
  it("Unauthorized", (done) => {
    request(app)
      .put("/user/myaccount/update")
      .send({
        name: "Mawang Yeager",
        age: 30,
        level: 99,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Invalid token", (done) => {
    request(app)
      .put("/user/myaccount/update")
      .set("authorization", [...token].reverse().join(""))
      .send({
        name: "Mawang Yeager",
        age: 30,
        level: 99,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});

describe("Delete Biodata Testing", () => {
  it("Success", (done) => {
    request(app)
      .delete("/user/myaccount/delete")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success delete Data Player.");
          done();
        }
      });
  });
  it("Unauthorized", (done) => {
    request(app)
      .delete("/user/myaccount/delete")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Invalid token", (done) => {
    request(app)
      .delete("/user/myaccount/delete")
      .set("authorization", [...token].reverse().join(""))
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});
