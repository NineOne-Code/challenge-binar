const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const bcrypt = require("bcryptjs");
const app = require("../app");

beforeAll((done) => {
  queryInterface
    .bulkInsert(
      "Users",
      [
        {
          id: 1,
          username: "ibrahim",
          password: bcrypt.hashSync("12345678", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    )
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

afterAll((done) => {
  queryInterface
    .bulkDelete("Users", null, { truncate: true, restartIdentity: true })
    .then(() => {
      done();
    })
    .catch((err) => {
      done();
    });
});

describe("Register Testing", () => {
  it("Success", (done) => {
    request(app)
      .post("/register")
      .send({
        username: "eren yeager",
        password: "yeager",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body).toHaveProperty("token");
          done();
        }
      });
  });

  it("No password", (done) => {
    request(app)
      .post("/register")
      .send({
        username: "eren",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(409);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Password required");
          done();
        }
      });
  });

  it("No username", (done) => {
    request(app)
      .post("/register")
      .send({
        password: "12345",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(409);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Username required");
          done();
        }
      });
  });
});

describe("Login Testing", () => {
  it("Success", (done) => {
    request(app)
      .post("/login")
      .send({
        username: "ibrahim",
        password: "12345678",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body).toHaveProperty("token");
          done();
        }
      });
  });

  it("Wrong password", (done) => {
    request(app)
      .post("/login")
      .send({
        username: "ibrahim",
        password: "rahasia",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(403);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Wrong Password");
          done();
        }
      });
  });

  it("Wrong username", (done) => {
    request(app)
      .post("/login")
      .send({
        username: "slebeuw",
        password: "12345678",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("User Not Found");
          done();
        }
      });
  });
});
