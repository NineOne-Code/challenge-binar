const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const app = require("../app");

require("dotenv").config();

let token;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync("12345678", salt);
  const user = {
    id: 1,
    username: "nine",
    password: hashPassword,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  await queryInterface.bulkInsert("Users", [user]);

  token = jwt.sign(
    {
      id: user.id,
      username: user.username,
    },
    process.env.JWT
  );
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "Users",
    {},
    { truncate: true, restartIdentity: true, cascade: true }
  );
});

describe("GET All Users Testing", () => {
  it("Success", (done) => {
    request(app)
      .get("/user")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success get All Players.");
          done();
        }
      });
  });
  it("Unauthorized", (done) => {
    request(app)
      .get("/user")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Invalid Token", (done) => {
    request(app)
      .get("/user")
      .set("authorization", [...token].reverse().join(""))
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});

describe("Delete Account Testing", () => {
  it("Success", (done) => {
    request(app)
      .delete("/user/delete")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success DELETE Account nine.");
          done();
        }
      });
  });

  it("No Auth", (done) => {
    request(app)
      .delete("/user/delete")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });

  it("Invalid Token", (done) => {
    request(app)
      .delete("/user/delete")
      .set("authorization", [...token].reverse().join(""))
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});
