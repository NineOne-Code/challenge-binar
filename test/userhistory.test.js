const request = require("supertest");
const { sequelize } = require("../models/index");
const { queryInterface } = sequelize;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const app = require("../app");

require("dotenv").config();

let token;
let id;

beforeEach(async () => {
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync("12345678", salt);
  const user = {
    id: 1,
    username: "nine",
    password: hashPassword,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  await queryInterface.bulkInsert("Users", [user]);

  token = jwt.sign(
    {
      id: user.id,
      username: user.username,
    },
    process.env.JWT
  );
  id = user.id;
});

afterEach(async () => {
  await queryInterface.bulkDelete(
    "Users",
    {},
    { truncate: true, restartIdentity: true, cascade: true }
  );
});

describe("GET All Histories Users Testing", () => {
  it("Unauthorized", (done) => {
    request(app)
      .get("/history")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("invalid token", (done) => {
    request(app)
      .get("/history")
      .set("authorization", [...token].reverse().join(""))
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Success", (done) => {
    request(app)
      .get("/history")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success get All Histories.");
          done();
        }
      });
  });
});

describe("GET History Testing", () => {
  it("Unauthorized", (done) => {
    request(app)
      .get("/history/myrecord")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("invalid token", (done) => {
    request(app)
      .get("/history/myrecord")
      .set("authorization", [...token].reverse().join(""))
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Success", (done) => {
    request(app)
      .get("/history/myrecord")
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success get Your Histories.");
          done();
        }
      });
  });
});

describe("POST History Testing", () => {
  it("Success", (done) => {
    request(app)
      .post("/history")
      .set("authorization", token)
      .send({
        name: "Eren Yeager",
        age: 18,
        level: 1,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success add Your Record.");
          done();
        }
      });
  });
  it("Unauthorized", (done) => {
    request(app)
      .post("/history")
      .send({
        name: "Eren Yeager",
        age: 18,
        level: 1,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Invalid token", (done) => {
    request(app)
      .post("/history")
      .set("authorization", [...token].reverse().join(""))
      .send({
        name: "Eren Yeager",
        age: 18,
        level: 1,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});

describe("Update History Testing", () => {
  it("Success", (done) => {
    request(app)
      .put(`/history/myrecord/${id}`)
      .set("authorization", token)
      .send({
        name: "Mawang Yeager",
        age: 30,
        level: 99,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success update Your Record.");
          done();
        }
      });
  });
  it("Unauthorized", (done) => {
    request(app)
      .put(`/history/myrecord/${id}`)
      .send({
        name: "Mawang Yeager",
        age: 30,
        level: 99,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Invalid token", (done) => {
    request(app)
      .put(`/history/myrecord/${id}`)
      .set("authorization", [...token].reverse().join(""))
      .send({
        name: "Mawang Yeager",
        age: 30,
        level: 99,
        gender: "m",
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});

describe("Delete History Testing", () => {
  it("Success", (done) => {
    request(app)
      .delete(`/history/myrecord/${id}`)
      .set("authorization", token)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Success delete Your Record.");
          done();
        }
      });
  });
  it("Unauthorized", (done) => {
    request(app)
      .delete(`/history/myrecord/${id}`)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
  it("Invalid token", (done) => {
    request(app)
      .delete(`/history/myrecord/${id}`)
      .set("authorization", [...token].reverse().join(""))
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body).toHaveProperty("message");
          expect(res.body.message).toBe("Unauthorized");
          done();
        }
      });
  });
});
