const express = require("express");
const {
  signIn,
  signUp,
  requestOTP,
  forgotPassword,
} = require("../controllers/auth.controller");
const router = express.Router();
const mailer = require("../helper/mailer");

router.post("/login", signIn);
router.post("/register", signUp);
router.post("/request-otp", requestOTP);
router.post("/forgot-password", forgotPassword);
// router.post("/test-mailer", mailer);
module.exports = router;
