const express = require("express");
const multer = require("../middlewares/multer");
const {
  addData,
  deleteData,
  getUser,
  updateData,
} = require("../controllers/userbiodata.controller");
const { authorization } = require("../middlewares/auth");

var router = express.Router();

// router.use(authorization);

router.get("/", getUser);
router.post("/add", multer.single("avatar"), addData);
router.delete("/delete", deleteData);
router.put("/update", multer.single("avatar"), updateData);
module.exports = router;
