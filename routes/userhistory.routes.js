const express = require("express");
const multer = require("../middlewares/multer");
const {
  getAllHistories,
  getHistory,
  addHistory,
  updateHistory,
  deleteHistory,
} = require("../controllers/userhistory.controller");
const { authorization } = require("../middlewares/auth");

var router = express.Router();

router.use(authorization);

router.get("/", getAllHistories);
router.post("/", multer.single("video"), addHistory);
router.get("/myrecord", getHistory);
router.put("/myrecord/:id", multer.single("video"), updateHistory);
router.delete("/myrecord/:id", deleteHistory);

module.exports = router;
